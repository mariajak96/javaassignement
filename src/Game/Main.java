package Game;

import Game.Heroes.Warrior;
import Game.Item.Armor;
import Game.Item.Slot;
import Game.Item.Weapon;

public class Main {

    public static void main(String[] args) throws Exception {
	//Enter the code here:

        Warrior thor = new Warrior("Hulk");

        Weapon mjolnir = new Weapon("Sword", 1, Slot.WEAPON, Weapon.Type.HAMMER,5, 4.5);
        thor.Equip(mjolnir);

        Armor chestPlate = new Armor("Hands", 1, Slot.BODY, Armor.Type.PLATE, 10, 2, 1);
        thor.Equip(chestPlate);


        thor.display();
    }
}
