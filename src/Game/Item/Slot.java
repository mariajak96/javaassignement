package Game.Item;

public enum Slot {

    HEAD,
    BODY,
    LEGS,
    WEAPON
}
