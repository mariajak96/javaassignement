package Game.Exceptions;

public class InvalidArmorException extends Exception{

    //Where we found our inspiration:  https://alvinalexander.com/java/java-custom-exception-create-throw-exception/
    public InvalidArmorException(String message) {
        super(message);
    }
}
