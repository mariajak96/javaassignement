package Game.Exceptions;

public class InvalidWeaponException extends Exception{

    //Where we found our inspiration: https://alvinalexander.com/java/java-custom-exception-create-throw-exception/
    public InvalidWeaponException(String message) {
        super(message);
    }
}
