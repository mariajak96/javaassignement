package Game.Heroes;

import Game.Exceptions.InvalidArmorException;
import Game.Exceptions.InvalidWeaponException;
import Game.Item.Armor;
import Game.Item.Weapon;

public class Warrior extends Character {

    public Warrior(String name) {
        super(name, 5, 2, 1);
        setCharacterDPS("strength");
    }

    //Sets new level and new attributes.
    public void levelup() {
        setLevel();
        updateBaseAttributes(3, 2, 1);
        //Sets CharacterDPS to re-calculate.
        setCharacterDPS("strength");
    }

    //Equipment -  Weapons.
    public void Equip(Weapon weapon) throws InvalidWeaponException {
        //Checks for the type of weapon, else throws.
        if (weapon.getWeaponType() == Weapon.Type.AXE || weapon.getWeaponType() == Weapon.Type.HAMMER || weapon.getWeaponType() == Weapon.Type.SWORD) {
            setEquipment(weapon);
            //Sets CharacterDPS to re-calculate.
            setCharacterDPS("strength");
        } else {
            throw new InvalidWeaponException(super.getName() + " can't equip " + weapon.getWeaponType() + " but can equip: Axes, Hammers and Swords.");
        }
    }

    //Equipment -  Armor.
    public void Equip(Armor armor) throws InvalidArmorException {
        //Checks for the type of armor, else throws.
        if (armor.getArmorType() == Armor.Type.MAIL || armor.getArmorType() == Armor.Type.PLATE) {
            setEquipment(armor);
            //Sets CharacterDPS to re-calculate.
            setCharacterDPS("strength");
        } else {
            throw new InvalidArmorException(super.getName() + " can't equip " + armor.getArmorType() + " but can equip: Mail, Plate.");
        }
    }
}
