package Game.Heroes;

import Game.Exceptions.InvalidArmorException;
import Game.Exceptions.InvalidWeaponException;
import Game.Item.Armor;
import Game.Item.Item;
import Game.Item.Slot;
import Game.Item.Weapon;

import java.util.HashMap;

public abstract class Character {
    //Fiends here:
    private final String name;
    private int level;
    private double characterDPS;
    private PrimaryAttribute basePrimaryAttributes;
    private PrimaryAttribute totalPrimaryAttributes;
    private HashMap<Slot, Item> equipment;

    //Consructor goes here.
    public Character(String name, int strength, int dexterity, int intelligence) {
        this.name = name;
        this.level = 1;
        this.basePrimaryAttributes = new PrimaryAttribute(strength, dexterity, intelligence); //set up PrimaryAttribute
        this.totalPrimaryAttributes = new PrimaryAttribute(); //set up empty total PrimaryAttribute
        this.equipment = new HashMap<>();
    }

    //Get.
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getBaseAttributes() {
        return this.basePrimaryAttributes.getAllAttributes();
    }

    public HashMap<Slot, Item> getEquipment() {

        //Go thrugh the equipments, remember to print the type and name.
        for (Slot i: this.equipment.keySet()) {
            System.out.println(i + " " + this.equipment.get(i).getName());
        }
        return equipment;
    }

    public String getTotalPrimaryAttributes() {

        //setGoal to check gear and newly added attributes
        updateTotalPrimaryAttributes();
        return totalPrimaryAttributes.getAllAttributes();
    }

    public double getCharacterDPS() {
        return characterDPS;
    }

    //setters
    public void setLevel(){
        this.level++;
    }

    public void setCharacterDPS(String main) {

        //Firstly, setGoal to check gear and newly added attributes
        updateTotalPrimaryAttributes();

        // Secondly, initialize mainAttributes.
        int mainAttribute = 0;
        //Check main attribute. Check get.
              if  (main.equals("strength")){
                  mainAttribute = totalPrimaryAttributes.getStrength();
              }  else if (main.equals("dexterity")) {
                  mainAttribute = totalPrimaryAttributes.getDexterity();
              } else if (main.equals("intelligence")) {
                  mainAttribute = totalPrimaryAttributes.getIntelligence();
              }

              //When the weapon slot is not empty.
        if (this.equipment.get(Slot.WEAPON) != null) {
            double weaponDPS = ((Weapon) this.equipment.get(Slot.WEAPON)).getDPS();
            this.characterDPS = weaponDPS * (1 + (mainAttribute / 100));
        } else {
            this.characterDPS = (1 + (mainAttribute / 100));
        }
    }

    //Methods here. When leveling up, new attributes are added.
    public void updateBaseAttributes(int strength, int dexterity, int intelligence) {
        this.basePrimaryAttributes.setAllAttributes(
                this.basePrimaryAttributes.getStrength() + strength,
                this.basePrimaryAttributes.getDexterity() + dexterity,
                this.basePrimaryAttributes.getIntelligence() + intelligence
        );
    }

    public void updateTotalPrimaryAttributes() {
        int strength = 0;
        int dexterity = 0;
        int intelligence = 0;
        //Looping through the gear character.
        for (Slot i: this.equipment.keySet()) {
            //Checks only for armor.
            if (i == Slot.HEAD || i == Slot.BODY || i == Slot.LEGS) {

                //Armor attributes in an array.
                String[] parts = ((Armor) this.equipment.get(i)).getArmorAttributes().split(",");
                //Attaining the right base attributes and indexes.
                strength = Integer.parseInt(parts[0]) + this.basePrimaryAttributes.getStrength();
                dexterity = Integer.parseInt(parts[1]) + this.basePrimaryAttributes.getDexterity();
                intelligence = Integer.parseInt(parts[2]) + this.basePrimaryAttributes.getIntelligence();
            }
        }
        this.totalPrimaryAttributes.setAllAttributes(strength, dexterity, intelligence);
    }

    public void setEquipment(Weapon weapon) throws InvalidWeaponException {
        // Checks the level, else throws.
        if (weapon.getRequiredLevel() <= this.level) {
            // Adding to hashmap.
            equipment.put(weapon.getSlot(), weapon);
        } else {
            throw new InvalidWeaponException("The level of this weapon is to high for " + this.getName() + " to equip.");
        }
    }

    public void setEquipment(Armor armor) throws InvalidArmorException {
        //Checks the level, else throws.
        if (armor.getRequiredLevel() <= this.level) {
            //Adding to hashmap.
            equipment.put(armor.getSlot(), armor);
        } else {
            throw new InvalidArmorException("The level of this armor is to high for " + this.getName() + " to equip.");
        }
    }

    //Returns a summary of characters.
    public void display() {
        StringBuilder charStats = new StringBuilder();

        charStats.append(
                "Character = " + this.getName() + '\n' +
                        "Class = " +this.getClass().getSimpleName() + '\n'+
                        "level = " + this.getLevel() + '\n' +
                        "Strength = " + this.totalPrimaryAttributes.getStrength() + '\n' +
                        "Dexterity = " + this.totalPrimaryAttributes.getDexterity() + '\n' +
                        "Intelligence = " + this.totalPrimaryAttributes.getIntelligence() + '\n' +
                        "characterDPS = " + characterDPS).toString();

        System.out.println(charStats);
    }
}
