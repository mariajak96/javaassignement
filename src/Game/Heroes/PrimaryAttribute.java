package Game.Heroes;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    //Constructor goes here.
    public PrimaryAttribute() {

    }

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    //Setter. Adds the passed arg. attributes.
    public void setAllAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //Getter. Attributes as a string by using "," in between.
    public String getAllAttributes() {
        return strength + "," + dexterity + "," + intelligence;
    }
}
