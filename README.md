# Role Play Game - Heroes. 
***
#### This code contains classes for creating and managing characters and items in a role-playing game. 

### Classes:<br>

Mage: Game character with magical attributes. 

Warrior: Character with physical combat abilities.

Armor: Armor that the characters can use for increased defense.

Weapon: Weapons that the characters can use for more effective attacks. 
***
# Usage:
### To use this package, you will need to import the necessary classes. For example:
***
#### Copy code:
    import Game.Characters.Mage;

    import Game.Characters.Ranger;

    import Game.Characters.Rogue;

    import Game.Characters.Warrior;
    
    import Game.Item.Armor;
    
    import Game.Item.Slot;
    
    import Game.Item.Weapon;


## Additional Notes
***
The display() method of the Warrior class can be used to display the character's current attributes, including their equipped items.